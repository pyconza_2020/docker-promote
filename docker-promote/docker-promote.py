import attr
import docker, docker.errors

DTR_NAME = "dtr.local"
SRC_PROJECT = "staging"
DST_PROJECT = "prod"

class DockerPromoteError(Exception):
    pass

@attr.s
class DockerPromote():
    name = attr.ib()
    version = attr.ib()
    debug = attr.ib(default=False)

    def __attrs_post_init__(self):
        self.docker_client = docker.from_env()
        self.src_tag = f"{DTR_NAME}/{SRC_PROJECT}/{self.name}:{self.version}"
        self.dst_tag = None
        self.img = None
        self.pull()
        self.tag()
        self.push()

    def pull(self):
        try:
            self.img = self.docker_client.images.pull(self.src_tag)
            if self.debug:
                print(f"Pulled {self.src_tag}")
        except Exception:
            raise DockerPromoteError(f"Error: Cannot pull {self.src_tag}")

    def tag(self):
        try:
            self.dst_tag = f"{DTR_NAME}/{DST_PROJECT}/{self.name}:{self.version}"
            self.img.tag(self.dst_tag)
        except Exception:
            raise DockerPromoteError(f"Error: Cannot tag {self.src_tag} as {self.dst_tag}")

    def push(self):
        if not self.dst_tag:
            raise DockerPromoteError(f"Error: No destination tag set")
        if not self.img:
            raise DockerPromoteError(f"Error: No image set")
        try:
            for line in self.docker_client.images.push(self.dst_tag, stream=True, decode=True):
                if self.debug and any([c in line.get("status", "") for c in ("Preparing", "Waiting", "Pushed", "digest")] + ["aux" in line]):
                    print(line, end="\n")
            if self.debug:
                print(f"Pushed {self.dst_tag}")
        except Exception:
            raise DockerPromoteError(f"Error: Cannot push {self.dst_tag}")

if __name__ == "__main__":
    from argparse_helper.argparse_helper import CommandLineParser

    clp = CommandLineParser(
        "Tool to promote docker images from staging to prod"
    )
    clp.add_quiet()
    clp.add_argument(
        "name", help="The name of the Docker image"
    )
    clp.add_argument(
        "version", help="The version of the Docker image"
    )
    clp.parse()

    try:
        db = DockerPromote(clp.name, clp.version, debug=clp.include_output)
    except DockerPromoteError as e:
        if clp.include_output:
            print(e)
